/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"jp/co/welport/JSDocSample/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});