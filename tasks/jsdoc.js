module.exports = function (grunt) {

	"use strict";
	grunt.config("jsdoc", {
		dist: {
			src: ["./webapp/*.js", "./webapp/controller/*.js"],
			options: {
				destination: "dist/doc"
			}
		}
	});

	grunt.loadNpmTasks("grunt-jsdoc");

};